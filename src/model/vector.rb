require_relative "./tuple"

class Vector < Tuple
  def initialize(x, y, z)
    super(x, y, z, 0)
  end

  def magnitude
    Math.sqrt(x**2 + y**2 + z**2)
  end

  def normalize
    mag = magnitude.to_f
    Vector.new(x / mag, y / mag, z / mag)
  end

  def dot(o)
    x * o.x +
      y * o.y +
      z * o.z +
      w * o.w
  end

  def cross(o)
    Vector.new(
      y * o.z - z * o.y,
      z * o.x - x * o.z,
      x * o.y - y * o.x
    )
  end
end
