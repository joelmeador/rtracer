require_relative "./tuple"

class Color < Tuple
  alias :red :x
  alias :green :y
  alias :blue :z

  def initialize(red, green, blue)
    super(red, green, blue, 0)
  end

  def *(o)
    if o.is_a?(Numeric)
      Color.new(red * o, green * o, blue * o)
    else
      Color.new(red * o.red, green * o.green, blue * o.blue)
    end
  end
end

class Color < Tuple
  Black = Color.new(0, 0, 0)
  Red   = Color.new(1, 0, 0)
end
