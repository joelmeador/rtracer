require_relative "./color"

class Canvas
  attr_accessor :width, :height, :pixels

  def initialize(width, height, initial_color = Color::Black)
    self.width = width
    self.height = height
    self.pixels = Array.new(width * height, initial_color)
  end

  def set(x, y, color)
    pixels[x + width * y] = color
  end

  def at(x, y)
    pixels[x + width * y]
  end

  def to_ppm
    PPMSerializer.new(self).serialize
  end
end

class PPMSerializer
  Version       = "P3"
  MaxColorValue = 255
  MaxLineLength = 70

  attr_accessor :canvas

  def initialize(canvas)
    self.canvas = canvas
  end

  def serialize
    header + body
  end

  private def header
    "#{Version}\n#{canvas.width} #{canvas.height}\n#{MaxColorValue}\n"
  end

  private def body
    out = ""
    line = ""
    canvas.pixels.each_with_index do |pixel, i|
      red = constrain(pixel.red).to_s
      green = constrain(pixel.green).to_s
      blue = constrain(pixel.blue).to_s
      if i > 0 && i % canvas.width == 0
        shorten(line, out)
        out << line.strip
        out << "\n"
        line = ""
      end
      line << red
      line << " "
      line << green
      line << " "
      line << blue
      line << " " unless (i + 1) % canvas.width == 0
    end
    shorten(line, out)
    out << line
    out << "\n"
    out
  end

  private def shorten(line, output)
    while line.length > MaxLineLength
      space_index = line[0..MaxLineLength].rindex(" ")
      output << line.slice!(0..space_index).strip
      output << "\n"
    end
  end

  private def constrain(pixel_value)
    return 0 if pixel_value < 0
    pixel_value = pixel_value * MaxColorValue
    pixel_value > MaxColorValue ? MaxColorValue : pixel_value.round
  end
end
