class Tuple
  Epsilon = 0.00001

  attr_accessor :x, :y, :z, :w

  def initialize(x, y, z, w)
    self.x = x
    self.y = y
    self.z = z
    self.w = w
  end

  def vector?
    !point?
  end

  def point?
    w == 1
  end

  def +(o)
    Tuple.new(x + o.x, y + o.y, z + o.z, w + o.w)
  end

  def -(o)
    Tuple.new(x - o.x, y - o.y, z - o.z, w - o.w)
  end

  def *(value)
    Tuple.new(x * value, y * value, z * value, w * value)
  end

  def /(value)
    t = value.to_f
    Tuple.new(x / t, y / t, z / t, w / t)
  end

  def ==(o)
    (x == o.x &&
     y == o.y &&
     z == o.z &&
     w == o.w) ||
      (eps_eq(x, o.x) &&
       eps_eq(y, o.y) &&
       eps_eq(z, o.z) &&
       eps_eq(w, o.w))
  end

  private def eps_eq(a, b)
    (a - b).abs < Epsilon
  end

  def negate
    Tuple.new(-x, -y, -z, -w)
  end

  def to_s
    %Q{x:#{"%0.2f" % x}, y:#{"%0.2f" % y}, z:#{"%0.2f" % z}, w:#{"%0.2f" % w}}
  end
end
