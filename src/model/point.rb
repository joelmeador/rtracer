require_relative "./tuple"

class Point < Tuple
  def initialize(x, y, z)
    super(x, y, z, 1)
  end
end
