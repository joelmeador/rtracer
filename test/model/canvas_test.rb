require "minitest/autorun"
require "minitest/pride"

require_relative "../../src/model/canvas"

describe Canvas do
  specify "new canvases are a grid of black" do
    canvas = Canvas.new(10, 20)
    assert_equal(10, canvas.width)
    assert_equal(20, canvas.height)
    black = Color::Black
    canvas.pixels.each do |pixel|
      assert_equal(black, pixel)
    end
  end

  it "can read pixels from a canvas" do
    canvas = Canvas.new(10, 20)
    red = Color::Red
    canvas.set(2, 3, red)
    assert_equal(Color::Red, canvas.at(2, 3))

  end

  describe "PPM" do
    specify "spits out a correct header" do
      canvas = Canvas.new(5, 3)
      ppm = canvas.to_ppm
      assert_equal(["P3", "5 3", "255"], ppm.split("\n").first(3))
    end

    specify "spits out pixel data" do
      canvas = Canvas.new(5, 3)
      canvas.set(0, 0, Color.new(1.5, 0, 0))
      canvas.set(2, 1, Color.new(0, 0.5, 0))
      canvas.set(4, 2, Color.new(-0.5, 0, 1))
      ppm = canvas.to_ppm
      assert_equal(
        ["255 0 0 0 0 0 0 0 0 0 0 0 0 0 0",
         "0 0 0 0 0 0 0 128 0 0 0 0 0 0 0",
         "0 0 0 0 0 0 0 0 0 0 0 0 0 0 255"],
        ppm.split("\n")[3..-1]
      )
    end

    specify "ppm ends with \n" do
      canvas = Canvas.new(5, 3)
      ppm = canvas.to_ppm
      assert_equal("\n", ppm[-1])
    end

    specify "long lines are split" do
      canvas = Canvas.new(10, 2, Color.new(1, 0.8, 0.6))
      ppm = canvas.to_ppm
      assert_equal(
        ["255 204 153 255 204 153 255 204 153 255 204 153 255 204 153 255 204",
         "153 255 204 153 255 204 153 255 204 153 255 204 153",
         "255 204 153 255 204 153 255 204 153 255 204 153 255 204 153 255 204",
         "153 255 204 153 255 204 153 255 204 153 255 204 153"],
        ppm.split("\n")[3..-1]
      )
    end
  end
end
