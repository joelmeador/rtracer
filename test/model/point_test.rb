require "minitest/autorun"
require "minitest/pride"

require_relative "../../src/model/point"

describe Point do
  it "is equivalent to tuples with same params and a 1 w component" do
    point = Point.new(1, 2, 3)
    tuple = Tuple.new(1, 2, 3, 1)
    assert_equal(point, tuple)
  end
end
