require "minitest/autorun"
require "minitest/pride"

require_relative "../../src/model/tuple"

describe Tuple do
  make_my_diffs_pretty!

  it "is a vector when given a w of 0" do
    vector = Tuple.new(4.3, -4.2, 3.1, 0)
    assert(vector.vector?)
    assert(!vector.point?)
  end

  it "is a point when give a w of 1" do
    point  = Tuple.new(4.3, -4.2, 3.1, 1)
    assert(!point.vector?)
    assert(point.point?)
  end

  it "supports negation" do
    tuple = Tuple.new(1, -2, 3, -4)
    expected = Tuple.new(-1, 2, -3, 4)
    assert_equal(expected, tuple.negate)
  end

  describe "addition" do
    it "adds a vector to a point to get another point" do
      tuple_1 = Tuple.new(3, -2, 5, 1)
      tuple_2 = Tuple.new(-2, 3, 1, 0)
      expected = Point.new(1, 1, 6)
      assert_equal(expected, tuple_1 + tuple_2)
    end
  end

  describe "subtraction" do
    it "returns a vector when a point is subtracted from another point" do
      point_1 = Point.new(3, 2, 1)
      point_2 = Point.new(5, 6, 7)
      expected = Vector.new(-2, -4, -6)
      assert_equal(expected, point_1 - point_2)
    end

    it "returns a point when subtracting a vector from a point" do
      point = Point.new(3, 2, 1)
      vector = Vector.new(5, 6, 7)
      expected = Point.new(-2, -4, -6)
      assert_equal(expected, point - vector)
    end

    it "returns a vector when subtracting two vectors" do
      vector_1 = Vector.new(3, 2, 1)
      vector_2 = Vector.new(5, 6, 7)
      expected = Vector.new(-2, -4, -6)
      assert_equal(expected, vector_1 - vector_2)
    end
  end

  describe "multiplication" do
    it "handles a scalar" do
      tuple = Tuple.new(1, 2, 3, 4)
      expected = Tuple.new(3.5, 7, 10.5, 14)
      assert_equal(expected, tuple * 3.5)
    end

    it "handles fractionals" do
      tuple = Tuple.new(1, 2, 3, 4)
      expected = Tuple.new(0.5, 1, 1.5, 2)
      assert_equal(expected, tuple * 0.5)
    end
  end

  describe "division" do
    it "handles scalar" do
      tuple = Tuple.new(1, 2, 3, 4)
      expected = Tuple.new(0.5, 1, 1.5, 2)
      assert_equal(expected, tuple / 2)
    end
  end

  describe "#dot" do
    it "computes it alright" do
      vector_1 = Vector.new(1, 2, 3)
      vector_2 = Vector.new(2, 3, 4)
      assert_equal(20, vector_1.dot(vector_2))
    end
  end

  describe "#cross" do
    it "computes it alright" do
      vector_1 = Vector.new(1, 2, 3)
      vector_2 = Vector.new(2, 3, 4)
      assert_equal(Vector.new(-1, 2, -1), vector_1.cross(vector_2))
      assert_equal(Vector.new(1, -2, 1), vector_2.cross(vector_1))
    end
  end
end
