require "minitest/autorun"
require "minitest/pride"

require_relative "../../src/model/color"

describe Color do
  it "has (red, green, and blue) tuples" do
    color = Color.new(-0.5, 0.4, 1.7)
    assert_equal(-0.5, color.red)
    assert_equal(0.4, color.green)
    assert_equal(1.7, color.blue)
  end

  it "supports addition" do
    color1 = Color.new(0.9, 0.6, 0.75)
    color2 = Color.new(0.7, 0.1, 0.25)
    assert_equal(Color.new(1.6, 0.7, 1), color1 + color2)
  end

  it "supports subtraction" do
    color1 = Color.new(0.9, 0.6, 0.75)
    color2 = Color.new(0.7, 0.1, 0.25)
    assert_equal(Color.new(0.2, 0.5, 0.5), color1 - color2)
  end

  describe "multiplication" do
    it "supports scalar" do
      color = Color.new(1, 0.2, 0.4)
      assert_equal(Color.new(2, 0.4, 0.8), color * 2)
    end

    it "support color multiplication" do
      color1 = Color.new(1, 0.2, 0.4)
      color2 = Color.new(0.9, 1, 0.1)
      assert_equal(Color.new(0.9, 0.2, 0.04), color1 * color2)
    end
  end
end
