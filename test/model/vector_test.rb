require "minitest/autorun"
require "minitest/pride"

require_relative "../../src/model/vector"

describe Vector do
  it "is equivalent to tuples with same params and a 0 w component" do
    vector = Vector.new(1, 2, 3)
    tuple = Tuple.new(1, 2, 3, 0)
    assert_equal(vector, tuple)
  end

  describe "#magnitude" do
    it "computes 1 for unit vectors" do
      assert_equal(1, Vector.new(1, 0, 0).magnitude, "x")
      assert_equal(1, Vector.new(0, 1, 0).magnitude, "y")
      assert_equal(1, Vector.new(0, 0, 1).magnitude, "z")
    end

    it "computes pythagorean distance non-unit vectors" do
      assert_equal(Math.sqrt(14), Vector.new(1, 2, 3).magnitude)
      assert_equal(Math.sqrt(14), Vector.new(-1, -2, -3).magnitude)
    end
  end

  describe "#normalize" do
    it "normalizes vectors using pythagorean distance" do
      assert_equal(Vector.new(4 / Math.sqrt(16), 0, 0), Vector.new(4, 0, 0).normalize)
      assert_equal(Vector.new(1 / Math.sqrt(14), 2 / Math.sqrt(14), 3 / Math.sqrt(14)),
                   Vector.new(1, 2, 3).normalize)
    end

    it "returns a vector with magnitude 1" do
      assert_equal(1, Vector.new(1, 2, 3).normalize.magnitude)
    end
  end
end
