require_relative "../src/model/point"
require_relative "../src/model/vector"

class Projectile
  attr_accessor :position, :velocity

  def initialize(position, velocity)
    self.position = position
    self.velocity = velocity
  end
end

class Environment
  attr_accessor :gravity, :wind

  def initialize(gravity, wind)
    self.gravity = gravity
    self.wind = wind
  end
end

class Timer
  def self.tick(environment, projectile)
    position = projectile.position + projectile.velocity
    velocity = projectile.velocity + environment.gravity + environment.wind
    Projectile.new(position, velocity)
  end
end

class Program
  def self.run
    # projectile starts one unit above the origin.
    # velocity is normalized to 1 unit/tick.
    projectile =  Projectile.new(Point.new(0, 1, 0), Vector.new(1, 1, 0).normalize)
    # gravity -0.1 unit/tick, and wind is -0.01 unit/tick.
    environment = Environment.new(Vector.new(0, -0.1, 0), Vector.new(-0.01, 0, 0))
    i = 0
    puts "starting position: #{projectile.position}"
    loop do
      i = i + 1
      projectile = Timer.tick(environment, projectile)
      puts "#{i}: #{projectile.position}"
      break if projectile.position.y <= 0
    end
  end
end

Program.run
