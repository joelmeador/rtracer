require_relative "../src/model/point"
require_relative "../src/model/vector"
require_relative "../src/model/canvas"

class Projectile
  attr_accessor :position, :velocity

  def initialize(position, velocity)
    self.position = position
    self.velocity = velocity
  end
end

class Environment
  attr_accessor :gravity, :wind

  def initialize(gravity, wind)
    self.gravity = gravity
    self.wind = wind
  end
end

class Timer
  def self.tick(environment, projectile)
    position = projectile.position + projectile.velocity
    velocity = projectile.velocity + environment.gravity + environment.wind
    Projectile.new(position, velocity)
  end
end

class Program
  def self.run
    # projectile starts one unit above the origin.
    # velocity is normalized to 1 unit/tick * k
    projectile =  Projectile.new(Point.new(0, 1, 0), Vector.new(1, 1, 0).normalize * 10)
    # gravity -0.1 unit/tick, and wind is -0.01 unit/tick.
    environment = Environment.new(Vector.new(0, -0.1, 0), Vector.new(-0.01, 0, 0))
    i = 0
    positions = []
    max_x = -1
    max_y = -1
    # simulate time passing
    loop do
      i = i + 1
      projectile = Timer.tick(environment, projectile)
      max_x = projectile.position.x if projectile.position.x > max_x
      max_y = projectile.position.y if projectile.position.y > max_y
      break if projectile.position.y <= 0
      positions << projectile.position
    end

    # give the canvas a little breathing room around the size of the image
    canvas = Canvas.new((max_x * 1.1).round, (max_y * 1.1).round)
    x_offset = (max_x * 0.05).round
    y_offset = (max_y * 0.05).round
    positions.each do |position|
      draw_pixel_circle(
        canvas,
        Point.new(x_offset + position.x, y_offset + position.y, 0)
      )
    end
    File.open("projectile.ppm", "w+") do |file|
      file.write(canvas.to_ppm)
    end
  end

  def self.draw_pixel_circle(canvas, point)
    center = Point.new(point.x.round, point.y.round, 0)
    canvas.set(center.x,     canvas.height - center.y,     Color::Red)
    canvas.set(center.x,     canvas.height - center.y - 1, Color::Red * 0.5)
    canvas.set(center.x,     canvas.height - center.y + 1, Color::Red * 0.5)
    canvas.set(center.x + 1, canvas.height - center.y,     Color::Red * 0.5)
    canvas.set(center.x - 1, canvas.height - center.y,     Color::Red * 0.5)
    canvas.set(center.x + 1, canvas.height - center.y + 1, Color::Red * 0.25)
    canvas.set(center.x - 1, canvas.height - center.y + 1, Color::Red * 0.25)
    canvas.set(center.x + 1, canvas.height - center.y - 1, Color::Red * 0.25)
    canvas.set(center.x - 1, canvas.height - center.y - 1, Color::Red * 0.25)
  end
end

Program.run
