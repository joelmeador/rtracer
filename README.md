Ray Tracer being built using the tutorial in The Ray Tracer Challenge by Jamis Buck.

----

Run tests: 
```sh
$ alias mt="time ruby -Ilib -e 'ARGV.each { |f| require f }' ./test/**/**test.rb"
$ mt
```

Run projectile programs
```sh
$ ruby program/projectile_fire_with_output.rb && open projectile.ppm
```